<?PHP
    class Login extends Controlador{
        
        function __Construct(){
            parent::__Construct();
        }
        
        public function Index(){
            $this->MostrarLogin();
        }
        
        public function MostrarLogin(){
            $Validacion=new NeuralJQueryValidacionFormulario;
            $Plantilla=new NeuralPlantillasTwig;
            echo $Plantilla->MostrarPlantilla('Login/VistaLogin1.html','JIGASOFT');
        }
        
        public function Verificacion(){
            $_POST=AyudasPost::FormatoEspacio($_POST);$_POST=AyudasPost::LimpiarInyeccionSQL($_POST);
            if(isset($_POST)){
                $Datos=$this->Modelo->VerificarUsuario($_POST);
                $Plantilla=new NeuralPlantillasTwig;                
                if(count($Datos)===0){	echo $Plantilla->MostrarPlantilla('Login/Error/ErrorLogin.html','JIGASOFT');
                }else{
                	session_start();
                    $Id=AyudasConversorHexAscii::ASCII_HEX(NeuralEncriptacion::EncriptarDatos($Datos[0]['ID_USUARIO'],'JIGASOFT'));
                    $_SESSION['Id']=$Id;
                	header('Location: '.NeuralRutasApp::RutaURL('Login/Acceso'));  
                }
            }
        }
        
        public function Acceso(){
            session_start();
            $Plantilla=new NeuralPlantillasTwig;
            if(isset($_SESSION['Id']) ){
                $A=AyudasSesion::DatosUsuarioSinSession();
                $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);                
                echo $Plantilla->MostrarPlantilla('Principal1.html','JIGASOFT');          
            }
            else{	echo $Plantilla->MostrarPlantilla('Login/Error/ErrorLogin.html','JIGASOFT'); }
        }
        
        public function Logout(){
            session_start();unset($_SESSION['Id']);session_destroy();header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
        }
    }