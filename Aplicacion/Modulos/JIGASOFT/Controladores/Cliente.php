<?PHP
    class Cliente extends Controlador{
        function __Construct(){
            parent::__Construct();
        }
        public function Index(){
            
        }
        
        public function MostrarClientes(){
            $A=AyudasSesion::DatosUsuario();
            $Datos=$this->Modelo->ConsultaClientes();
            for($i=0;$i<count($Datos);$i++){
                $Datos[$i][5]=AyudasConversorHexAscii::ASCII_HEX(NeuralEncriptacion::EncriptarDatos($Datos[$i]['IdCliente'],'JIGASOFT'));
            }
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            $Plantilla->ParametrosEtiquetas('reg',$Datos);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaClientes.html','JIGASOFT');            
        }
        
        public function MostrarRegistrar(){
            $A=AyudasSesion::DatosUsuario();
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaRegistro.html','JIGASOFT');            
        }
        
        public function Registrar(){
            $_POST=AyudasPost::FormatoEspacio($_POST);
            $this->Modelo->RegistrarCliente($_POST);
            $this->MostrarClientes();
        }
        
        public function MostrarModificar($Id){
            $Datos=$this->Modelo->ObtenerCliente($Id);
            $A=AyudasSesion::DatosUsuario();
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            $Plantilla->ParametrosEtiquetas('Arre',$Datos[0]);
            $Plantilla->ParametrosEtiquetas('Id',$Id);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaModificacion.html','JIGASOFT');            
        }
        public function Modificar(){
            $_POST=AyudasPost::FormatoEspacio($_POST);
            $this->Modelo->ActualizarCliente($_POST);
            $this->MostrarClientes();
        }
        public function MostrarNuevoContacto($Id){
            $Datos=$this->Modelo->Municipio();
            $A=AyudasSesion::DatosUsuario();
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            $Plantilla->ParametrosEtiquetas('Muni',$Datos);
            $Plantilla->ParametrosEtiquetas('Id',$Id);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaRegistroContacto.html','JIGASOFT');            
        }
        public function RegistrarContacto(){
            $_POST=AyudasPost::FormatoEspacio($_POST);
            $this->Modelo->RegistrarContacto($_POST);
            $this->MostrarContactos();
        }
        public function MostrarContactos(){
            $A=AyudasSesion::DatosUsuario();
            $Datos=$this->Modelo->ConsultarContactos();
            for($i=0;$i<count($Datos);$i++){
                $Datos[$i][10]=AyudasConversorHexAscii::ASCII_HEX(NeuralEncriptacion::EncriptarDatos($Datos[$i]['IdContactoCliente'],'JIGASOFT'));
            }
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            $Plantilla->ParametrosEtiquetas('reg',$Datos);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaContactos.html','JIGASOFT'); 
        }
        
        public function MostrarModificarContacto($Id){
            $Datos=$this->Modelo->ObtenerContacto($Id);
            $Muni=$this->Modelo->Municipio();
            $Clien=$this->Modelo->ConsultaClientes();
            $A=AyudasSesion::DatosUsuario();
            $Plantilla=new NeuralPlantillasTwig;
            $Plantilla->ParametrosEtiquetas('Usuario',$A[0]['Nombre']);
            $Plantilla->ParametrosEtiquetas('Arre',$Datos[0]);
            $Plantilla->ParametrosEtiquetas('Muni',$Muni);
            $Plantilla->ParametrosEtiquetas('Clien',$Clien);
            $Plantilla->ParametrosEtiquetas('Id',$Id);
            echo $Plantilla->MostrarPlantilla('Cliente/VistaModificarContacto.html','JIGASOFT');            
        }
        public function ModificarContacto(){
            $_POST=AyudasPost::FormatoEspacio($_POST);
            $this->Modelo->ActualizarContacto($_POST);
            $this->MostrarContactos();
        }
    }
?>