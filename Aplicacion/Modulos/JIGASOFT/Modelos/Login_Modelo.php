<?PHP
    class Login_Modelo extends Modelo{
        
        function __Construct(){
            parent::__Construct();
        }
        
        public function Index(){
            
        }
        
        public function VerificarUsuario($ArrayDatos){
            $ArrayDatos=AyudasPost::FormatoEspacio($ArrayDatos);
            $ArrayDatos=AyudasPost::LimpiarInyeccionSQL($ArrayDatos);
            $password=AyudasConversorHexAscii::ASCII_HEX(NeuralEncriptacion::EncriptarDatos($ArrayDatos['Password'],'JIGASOFT'));
            $sql="SELECT A.IdUsuario AS ID_USUARIO,CONCAT(A.Nombre,' ',A.ApellidoPaterno,' ',A.ApellidoMaterno) AS Nombre,A.Cargo,A.`Status`
			FROM tbl_usuario A
			WHERE A.Usuario='".$ArrayDatos['Usuario']."' AND A.Password='".$password."' AND A.`Status`='1';";
            $consulta=new NeuralBDConsultas;
            $consulta->PrepararQuery();
            return $consulta->ExecuteQueryManual('JIGASOFT',$sql);
        }    
    }