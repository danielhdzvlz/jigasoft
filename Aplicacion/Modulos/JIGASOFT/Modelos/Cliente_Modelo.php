<?php
    class Cliente_Modelo extends Modelo{
        
        function __Construct(){
            parent::__Construct();
        }
        
        public function ConsultaClientes(){
            $Consulta=new NeuralBDConsultas;
            $Consulta->PrepararQuery();
            return $Consulta->ExecuteQueryManual('JIGASOFT','SELECT a.Nombre,a.Telefono,a.Celular,a.Correo,a.`Status`,a.IdCliente FROM tbl_cliente a GROUP BY a.Nombre;');
        }
        
        public function RegistrarCliente($Arre){
            $Arre=AyudasPost::LimpiarInyeccionSQL($Arre);
            $sql="INSERT INTO tbl_cliente(
            tbl_cliente.Nombre,
            tbl_cliente.Telefono,
            tbl_cliente.Celular,
            tbl_cliente.Correo,
            tbl_cliente.`Status`)
            VALUES(
            '".$Arre['Nombres']."',
            '".$Arre['Telefono']."',
            '".$Arre['Celular']."',
            '".$Arre['Correo']."',
            '".$Arre['Status']."'
            );";
            AyudasSQL::InsertSQL('JIGASOFT',$sql);
        }
        
        public function ObtenerCliente($Id){
            $sql="SELECT a.Nombre,a.Telefono,a.Celular,a.Correo,a.`Status` FROM tbl_cliente a WHERE a.IdCliente=".NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($Id),'JIGASOFT').";";
            $consulta=new NeuralBDConsultas;
            $consulta->PrepararQuery();
            return $consulta->ExecuteQueryManual('JIGASOFT',$sql);
        }
        
        public function ActualizarCliente($Arre){
            $sql="UPDATE tbl_cliente A
            SET
            A.Nombre='".$Arre['Nombres']."',
            A.Telefono='".$Arre['Telefono']."',
            A.Celular='".$Arre['Celular']."',
            A.Correo='".$Arre['Correo']."',
            A.`Status`='".$Arre['Status']."'
            WHERE 
            A.IdCliente=".NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($Arre['Id']),'JIGASOFT').";";
            AyudasSQL::UpdateSQL('JIGASOFT',$sql);
        }
        
        public function Municipio(){
            $sql="SELECT A.IdMunicipio,A.Municipio FROM catalogo_municipio A GROUP BY A.Municipio;";
            $consulta=new NeuralBDConsultas;
            $consulta->PrepararQuery();
            return $consulta->ExecuteQueryManual('JIGASOFT',$sql);
        }
        
        public function RegistrarContacto($Arre){
            $sql="INSERT INTO tbl_contacto_cliente(
				tbl_contacto_cliente.Alias,
				tbl_contacto_cliente.NombreFiscal,
				tbl_contacto_cliente.Direccion,
				tbl_contacto_cliente.Telefono,
				tbl_contacto_cliente.Correo,
				tbl_contacto_cliente.Ciudad,
				tbl_contacto_cliente.CP,
				tbl_contacto_cliente.RFC,
				tbl_contacto_cliente.IdMunicipio,
				tbl_contacto_cliente.IdCliente
            )
            VALUES(
            '".$Arre['Alias']."',
            '".$Arre['Nombres']."',
            '".$Arre['Direccion']."',
            '".$Arre['Telefono']."',
            '".$Arre['Correo']."',
            '".$Arre['Ciudad']."',
            '".$Arre['CP']."',
            '".$Arre['RFC']."',
            '".$Arre['Municipio']."',
            '".NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($Arre['Id']),'JIGASOFT')."'
            );";
            AyudasSQL::InsertSQL('JIGASOFT',$sql);
        }
        
        public function ConsultarContactos(){
            $sql="SELECT A.Alias,A.NombreFiscal,A.Direccion,A.Telefono,A.Correo,A.Ciudad,A.CP,A.RFC,C.Municipio,B.Nombre,A.IdContactoCliente
            FROM tbl_contacto_cliente A,tbl_cliente B,catalogo_municipio C
            WHERE A.IdCliente=B.IdCliente AND A.IdMunicipio=C.IdMunicipio
            GROUP BY A.Alias;";
            $consulta=new NeuralBDConsultas;
            $consulta->PrepararQuery();
            return $consulta->ExecuteQueryManual('JIGASOFT',$sql);
        }
        public function ObtenerContacto($Id){
            $sql="SELECT A.Alias,A.NombreFiscal,A.Direccion,A.Telefono,A.Correo,A.Ciudad,A.CP,A.RFC,C.IdMunicipio,C.Municipio,B.IdCliente,B.Nombre,A.IdContactoCliente
            FROM tbl_contacto_cliente A,tbl_cliente B,catalogo_municipio C
            WHERE A.IdCliente=B.IdCliente AND A.IdMunicipio=C.IdMunicipio AND A.IdContactoCliente='".NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($Id),'JIGASOFT')."';";
            $consulta=new NeuralBDConsultas;
            $consulta->PrepararQuery();
            return $consulta->ExecuteQueryManual('JIGASOFT',$sql);
        }
        public function ActualizarContacto($Arre){
            $sql="UPDATE tbl_contacto_cliente
            SET
				tbl_contacto_cliente.Alias='".$Arre['Alias']."',
				tbl_contacto_cliente.NombreFiscal='".$Arre['Nombres']."',
				tbl_contacto_cliente.Direccion='".$Arre['Direccion']."',
				tbl_contacto_cliente.Telefono='".$Arre['Telefono']."',
				tbl_contacto_cliente.Correo='".$Arre['Correo']."',
				tbl_contacto_cliente.Ciudad='".$Arre['Ciudad']."',
				tbl_contacto_cliente.CP='".$Arre['CP']."',
				tbl_contacto_cliente.RFC='".$Arre['RFC']."',
				tbl_contacto_cliente.IdMunicipio='".$Arre['Municipio']."',
				tbl_contacto_cliente.IdCliente='".$Arre['Cliente']."'
            
            WHERE tbl_contacto_cliente.IdContactoCliente='".NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($Arre['Id']),'JIGASOFT')."';";
            AyudasSQL::InsertSQL('JIGASOFT',$sql);
        }
    }
?>