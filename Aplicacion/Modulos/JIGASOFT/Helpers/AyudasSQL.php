<?PHP
    abstract class AyudasSQL{
        
        public static function InsertSQL($Aplicacion=false,$querySQL=false){
            if(isset($Aplicacion) AND isset($querySQL)){
                    $Conexion=new NeuralConexionBaseDatos;
                    $Insert=$Conexion->ObtenerConexionBase($Aplicacion);
                    $Insert->query($querySQL);
            }
            
        }
        
        public static function UpdateSQL($Aplicacion=false,$querySQL=false){
            if(isset($Aplicacion) AND isset($querySQL)){
                    $Conexion=new NeuralConexionBaseDatos;
                    $Update=$Conexion->ObtenerConexionBase($Aplicacion);
                    $Update->query($querySQL);
            }
        }
        
        public static function SelectSQL($Aplicacion=false,$querySQL=false){
            if(isset($Aplicacion) AND isset($querySQL)){
                    $Conexion=new NeuralConexionBaseDatos;
                    $Select=$Conexion->ObtenerConexionBase($Aplicacion);
                    return $Select->query($querySQL);
            }
            
        }
        
        public static function InsertVariosRegistrosSQL($Aplicacion=false,$querySQL=false){
            if(isset($Aplicacion) AND isset($querySQL)){
                    $Conexion=new NeuralConexionBaseDatos;
                    $Insert=$Conexion->ObtenerConexionBase($Aplicacion);
                    $Insert->query('"'.$querySQL.'"');
            }
            
        }
        
        
    }