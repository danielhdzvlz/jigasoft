<?php
	
	class AyudasSesion {
	
		public static function UsuarioSesion() {
			if( session_start() == true AND isset($_SESSION['Dato']) ) {
				$User=$_SESSION['Dato'];
				return $User;
			}elseif( !isset($_SESSION['Dato']) ){
				header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
			}
				//return $User;
		}
        
		public static function CargoSesion() {
			if( isset($_SESSION['Cargo']) ) {
				$User=$_SESSION['Cargo'];
				return $User;
			}elseif( !isset($_SESSION['Cargo']) ){
				header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
			}
				//return $User;
		}
        
        public static function Usuario() {
			if(isset($_SESSION['Dato']) ) {
				$User=$_SESSION['Dato'];
				return $User;
			}elseif( !isset($_SESSION['Dato']) ){
				header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
			}
				//return $User;
		}
        
        public static function IdUsuarioSesion() {
			if( isset($_SESSION['Id']) ) {
				$User=$_SESSION['Id'];
				return $User;
			}elseif( !isset($_SESSION['Id']) ){
				header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
			}
				//return $User;
		}
        public static function FechaHora(){
            $ZonaHoraria='America/Mexico_City';
            date_default_timezone_set($ZonaHoraria);
            $hoy = getdate();         
            $año=$hoy['year'];
            $mes=$hoy['mon'];
            $dia=$hoy['mday'];
            $hora=$hoy['hours'];
            $minuto=$hoy['minutes'];
            $segundo=$hoy['seconds'];
            $Hor=$hora.':'.$minuto.':'.$segundo;
            $Fecha=$año.'-'.$mes.'-'.$dia;
            $FechaReal=$Fecha.' '.$Hor;
            return $FechaReal;



 
        }
        
        public static function FechaActual(){
            $ZonaHoraria='America/Mexico_City';
            date_default_timezone_set($ZonaHoraria);
            $hoy = getdate();
            $año=$hoy['year'];
            $mes=$hoy['mon'];
            $dia=$hoy['mday'];
            $Fecha=$año.'-'.$mes.'-'.$dia;
            return $Fecha;
        }
        
        public static function FormatoFecha($fecha){
            return date('Y-m-d',strtotime($fecha));
        }
        
        public static function FormatoFechaHora($fecha){
            return date('Y-m-d H:i:s',strtotime($fecha));
        }
        
        
        
        public static function IdSesion() {
			if( session_start() == true AND isset($_SESSION['Id']) ) {
				$User=$_SESSION['Id'];
				return $User;
			}elseif( !isset($_SESSION['Id']) ){
				header('Location: '.NeuralRutasApp::RutaURL('Login/Index'));
			}
				//return $User;
		}
        
        public static function DatosUsuario(){
            if(session_start()==true){
                if(isset($_SESSION['Id'])){
                    /** POR EL MOMENTO EL CIFRADO ESTARÁ DESACTIVADO HASTA QUE SE CONFIGURE HE INSTALE EL MYSCRY*/
                    $Id=NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($_SESSION['Id']),'JIGASOFT');
                    //$Id=$_SESSION['Id'];
                    $sql="SELECT A.IdUsuario,CONCAT(A.Nombre,' ',A.ApellidoPaterno,' ',A.ApellidoMaterno) AS Nombre,A.Cargo,A.`Status`
				FROM tbl_usuario A
				WHERE A.IdUsuario='".$Id."';";
                    $Consulta=new NeuralBDConsultas;
                    $Consulta->PrepararQuery();
                    $User=$Consulta->ExecuteQueryManual('JIGASOFT',$sql);
                    return $User;
                }else{
                    echo 'error datos';
                }
            }else{
                echo 'no se ha iniciado session';
            }
        }
        public static function DatosUsuarioSinSession(){
            if(isset($_SESSION['Id'])){
                /** POR EL MOMENTO EL CIFRADO ESTARÁ DESACTIVADO HASTA QUE SE CONFIGURE HE INSTALE EL MYSCRY*/
                $Id=NeuralEncriptacion::DesencriptarDatos(AyudasConversorHexAscii::HEX_ASCII($_SESSION['Id']),'JIGASOFT');
                //$Id=$_SESSION['Id'];
                $sql="SELECT A.IdUsuario,CONCAT(A.Nombre,' ',A.ApellidoPaterno,' ',A.ApellidoMaterno) AS Nombre,A.Cargo,A.`Status`
				FROM tbl_usuario A
				WHERE A.IdUsuario='".$Id."';";
                $Consulta=new NeuralBDConsultas;
                $Consulta->PrepararQuery();
                $User=$Consulta->ExecuteQueryManual('JIGASOFT',$sql);
                return $User;
            }else{
                echo 'error datos';
            }           
        }
	}
        
        